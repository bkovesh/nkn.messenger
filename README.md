## NKN.Messenger

Decentralized p2p messenger. It uses React Native, NKN, IPFS, Ethereum smart contracts, Infura.

### Watch the video ↓↓↓

[![Watch the video](./Screenshot.jpg)](https://youtu.be/_f01UXAUhjg)


### Commands to start:

Install EXPO globally:
```
npm i exp -g
```

Install all project dependencies:
```
npm i
```

Run project:
```
npm run start
```


### Command to run test:
```
npm run test
```