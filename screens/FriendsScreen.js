import React from 'react';
import { ScrollView, StyleSheet, View, TextInput, Text, FlatList, Image, AsyncStorage, TouchableOpacity, Platform } from 'react-native';
import { List, ListItem, Button, Divider, Card } from 'react-native-elements';
import Colors from '../constants/Colors';
import { ethContractFunc, ethUploadFile, ethAbiParse } from '../helpers/ethApi';
import { REGISTRY_CONTRACT_ADDRESS } from 'react-native-dotenv'

export default class FriendsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textSearch: '',
      friends: [
        { name: 'Fedot', nknAddr: 'addressNkn', chatHistory: 'hashIpfs', avatar: 'QmdAVdyJYV8DYrG13pZUvf8ZZah3btk8VfE4iVbbegoqtF', desc: 'I am Fedot from Murom.' },
        { name: 'Yaropolk', nknAddr: 'addressNkn', chatHistory: 'hashIpfs', avatar: 'QmU3hwXeePd2cQv6utvG2K4sfGFr5HFefdKtMaNFAU3Wsj', desc: 'I am Yaropolk from Yaroslavl.' },
      ],
      foundUser: []
    };
  }

  static navigationOptions = {
    title: 'Friends',
  };

  render() {
    const {foundUser,friends} = this.state
    return (
      <ScrollView style={styles.container}>
        {/* Search */}
        <View style={styles.inputContainer}>
          <Text style={styles.labelText}>
            Search
          </Text>
          <TextInput
            style={styles.inputText}
            value={this.state.textSearch}
            autoFocus={true}
            onChangeText={(textSearch) => {
              this._searchUserByName()
              this.setState({textSearch})
            }}
          />
        </View>


        {/* Search Result*/}
        <View style={styles.resultContainer}>
          <Text style={{textAlign:'center'}}>────────  Found user  ────────</Text>
        </View>
        <View style={styles.card}>
          {
            (foundUser && foundUser.name) ?
            <TouchableOpacity key={0} style={styles.cardItem} onPress={() => {
              let user = { name: foundUser[0], addressNkn: foundUser[2], avatar: foundUser[3], desc: foundUser[4] }
              this._lookAtUser(user)
            }}>
              <Image
                style={styles.avatar}
                resizeMode="cover"
                source={{ uri: 'https://gateway.ipfs.io/ipfs/'+foundUser[3] }}
              />
            <Text style={{marginLeft:10}}>{foundUser[0]}</Text>
            </TouchableOpacity> :
            <View key={0} style={styles.cardItem}>
              <Text style={{}}>Result will be here</Text>
            </View>
          }
        </View>

        {/* My friends */}
        <View style={styles.resultContainer}>
          <Text style={{textAlign:'center'}}>────────   My friends  ────────</Text>
        </View>
        <View style={styles.card}>
          {
            (friends) ?
            friends.map((u, i) => {
              return (
                <TouchableOpacity key={i} style={styles.cardItem} onPress={() => {
                  this._lookAtUser(u)
                }}>
                  <Image
                    style={styles.avatar}
                    resizeMode="cover"
                    source={{ uri: 'https://gateway.ipfs.io/ipfs/'+u.avatar }}
                  />
                  <Text style={{marginLeft:10}}>{u.name}</Text>
                </TouchableOpacity>
              );
            }) :
            <View key={0} style={styles.cardItem}>
            </View>
          }
        </View>


      </ScrollView>
    );
  }


  _searchUserByName = async () => {
    try {
      // TODO: get ipfsUserUrl from contract by name
      console.log('Search');
      const ethPrivateKey = await AsyncStorage.getItem('@NknStore:ethPrivateKey');
      // console.log('ethPrivateKey',ethPrivateKey);
      const foundUser = await ethContractFunc(ethPrivateKey, REGISTRY_CONTRACT_ADDRESS, false, 'showUserByName', [this.state.textSearch]).then((res) => {
        this.setState({foundUser:res})
        return res
      })
      console.log('foundUser',foundUser);
      return foundUser
    } catch (err) {
      console.log(err);
      return err
    }
    // TODO: fetch userObj from ipfs
    // TODO: parse userObj to list
  }


  _lookAtUser = async (user) => {
    try {
      // TODO: routes to userScreen
      console.log(user);
      await AsyncStorage.setItem('@NknStore:lookAtUser', user.name); // TODO: delete
      this.props.navigation.navigate('User', user)
    } catch (e) {
      console.log(e);
    }
  }


  componentWillMount = async () => {
    const logged = await AsyncStorage.getItem('@NknStore:logged');
    const name = await AsyncStorage.getItem('@NknStore:name');
    const nknAddr = await AsyncStorage.getItem('@NknStore:nknAddr');
    const address = await AsyncStorage.getItem('@NknStore:ethAddress');
    const privateKey = await AsyncStorage.getItem('@NknStore:ethPrivateKey');
    const chatHistory = await AsyncStorage.getItem('@NknStore:chatHistory');
    const widgets = await AsyncStorage.getItem('@NknStore:widgets');
    // const friends = await AsyncStorage.getItem('@NknStore:friends');

    this.setState({
      logged: Boolean(logged),
      name: name,
      nknAddr: nknAddr,
      address: address,
      privateKey: privateKey,
      chatHistory: chatHistory,
      widgets: widgets,
      // friends: JSON.parse(friends),
    })
  }


}




const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  card: {
    marginTop: 10,
    padding: 10,
    width: '90%',
    // alignItems: 'center',
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 2,
      },
      android: {
        elevation: 10,
      },
    }),
  },
  cardItem: {
    flexDirection: 'row',
    margin: 10,
  },
  avatar: {
    width: 30,
    height: 30,
    borderRadius: 15,
    borderColor: 'rgba(0,0,0,0.1)',
    borderWidth: 1,
    padding: 10,
    marginBottom: 0,
  },
  requestContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  resultContainer: {
    marginTop: 30,
    alignItems: 'center',
  },
  inputText: {
    alignItems: 'center',
    textAlign: 'center',
    height: 40,
    width: '80%',
    borderColor: 'gray',
    borderWidth: 0
  },
  inputContainer: {
    width: '100%',
    alignItems: 'center',
  },
  labelText: {
    fontSize: 12,
    color: 'gray',
  },
});
