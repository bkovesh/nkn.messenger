import React from 'react';
import {
  Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View, TextInput, Button, AsyncStorage
} from 'react-native';
import { WebBrowser } from 'expo';
import { MonoText } from '../components/StyledText';
import LabelInput from '../components/LabelInput';
import { publicFileToIpfs } from '../helpers/ipfsApi';
import { genWallet, ethContractFunc, ethDownloadKeyFile } from '../helpers/ethApi';
import { nknConnect } from '../helpers/nknApi';
import { REGISTRY_CONTRACT_ADDRESS } from 'react-native-dotenv'
import nkn from 'nkn-client'


export default class LoginScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      logged: false,
      inpNameUniq: true,
      inpName: 'Boris',
      inpAddr: '0xcd2e82ee657ec75aa3d7c296255ff1c75c0a9482',
      inpKey: '0x73E9DE34C6BEF463BD1B1AB621235FC780ED9B901FC423164C50C546991C94E5',
      avatar: 'QmdAVdyJYV8DYrG13pZUvf8ZZah3btk8VfE4iVbbegoqtF',
    };
  }

  static navigationOptions = {
    header: null,
  };

  render() {

    let containerIfLogged
    if (this.state.logged === 'true') {
      containerIfLogged = (
        <View style={styles.helpContainer}>
          <TouchableOpacity onPress={this._exit} style={styles.helpLink}>
            <Text style={styles.helpLinkText}>Exit</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._forgetAccount} style={styles.helpLink}>
            <Text style={styles.helpLinkText}>Forget account</Text>
          </TouchableOpacity>
        </View>
      )
    } else {
      containerIfLogged = (
        <View style={styles.helpContainer}>

          <View style={styles.inputContainer}>
            <Text style={styles.labelText}>
              Name
            </Text>
            <TextInput
              multiline = {true}
              style={styles.inputName}
              value={this.state.inpName}
              onChangeText={(inpName) => {
                let uniq = this._isUserUniq(inpName)
                this.setState({inpName,inpNameUniq:uniq})
              }}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.labelText}>
              Ethereum Address
            </Text>
            <TextInput
              multiline = {true}
              numberOfLines = {3}
              style={styles.inputTextMultiline}
              value={this.state.inpAddr}
              onChangeText={(inpAddr) => this.setState({inpAddr})}
            />
          </View>

          <View style={styles.inputContainer}>
            <Text style={styles.labelText}>
              Ethereum Private key
            </Text>
            <TextInput
              multiline = {true}
              numberOfLines = {3}
              style={styles.inputTextMultiline}
              value={this.state.inpKey}
              onChangeText={(inpKey) => this.setState({inpKey})}
            />
          </View>

          <TouchableOpacity onPress={this._changeAvatar} style={styles.helpLink}>
            <Text style={styles.helpLinkText}>Set avatar</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._login} style={styles.helpLink}>
            <Text style={styles.helpLinkText}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._genWallet} style={styles.helpLink}>
            <Text style={styles.helpLinkText}>Generate new wallet</Text>
          </TouchableOpacity>

          <View style={{marginTop: 100}}>
            <TouchableOpacity onPress={this._help1} style={styles.helpLink}>
              <Text style={styles.helpLinkText}>set account1</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this._help2} style={styles.helpLink}>
              <Text style={styles.helpLinkText}>set account2</Text>
            </TouchableOpacity>
          </View>
        </View>
      )
    }

    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.welcomeContainer}>
            <Image
              source={require('../assets/images/nkn-logo.png')}
              style={styles.welcomeImage}
            />
          </View>

          <View style={styles.getStartedContainer}>
            <Text style={styles.developmentModeText}>
              NKN.Messenger
            </Text>
          </View>

          {containerIfLogged}

        </ScrollView>
      </View>
    );
  }


  _login = async () => {
    try {
      const {inpName, inpAddr, inpKey, avatar} = this.state

      const nknClient = await nkn({
        identifier: inpAddr,
        privateKey: inpKey,
        seedRpcServerAddr: 'http://104.196.247.255:30003',
      });
      this.setState({
        nknClient: nknClient
      })
      console.log('_login', avatar);

      await AsyncStorage.setItem('@NknStore:logged', 'true');
      await AsyncStorage.setItem('@NknStore:name', inpName);
      await AsyncStorage.setItem('@NknStore:avatar', avatar);
      await AsyncStorage.setItem('@NknStore:nknAddr', nknClient.addr);
      await AsyncStorage.setItem('@NknStore:ethAddress', inpAddr);
      await AsyncStorage.setItem('@NknStore:ethPrivateKey', inpKey);
      console.log('_login', nknClient.addr);

      ethContractFunc(inpKey, REGISTRY_CONTRACT_ADDRESS, true, 'addUser', [inpName, inpAddr, nknClient.addr, avatar, 'description'])
      console.log('_login', nknClient.addr);

      this.setState({logged: true})
      this.forceUpdate()
      const user = { name: inpName, addressNkn: nknClient.addr, avatar: avatar, desc: '' }
      this.props.navigation.navigate('User', user);
    } catch (err) {
      console.error('_login',err);
    }
  }


  _exit = async () => {
    await AsyncStorage.setItem('@NknStore:logged', 'false');
    this.setState({logged: false})
    this.forceUpdate()
  }


  _forgetAccount = async () => {
    await AsyncStorage.setItem('@NknStore:logged', 'false');
    await AsyncStorage.setItem('@NknStore:name', '');
    await AsyncStorage.setItem('@NknStore:avatar', '');
    await AsyncStorage.setItem('@NknStore:nknAddr', '');
    await AsyncStorage.setItem('@NknStore:ethAddress', '');
    await AsyncStorage.setItem('@NknStore:ethPrivateKey', '');
    await AsyncStorage.setItem('@NknStore:chatHistory', '');
    await AsyncStorage.setItem('@NknStore:widgets', '');
    await AsyncStorage.setItem('@NknStore:friends', '');
    this.setState({logged: false})
    this.forceUpdate()
  }


  _isUserUniq = async (name) => {
    try {
      const {inpKey} = this.state
      console.log('uniq?');
      const user = await ethContractFunc(inpKey, REGISTRY_CONTRACT_ADDRESS, false, 'isUsernameUniq', [name])
      //TODO: return bool
      if (user[0] === '0x0000000000000000000000000000000000000000'){
        this.setState({inpNameUniq:true})
        console.log('is uniq');
        return true
      } else {
        this.setState({inpNameUniq:false})
        console.log('is not uniq');
        return false
      }
    } catch (e) {
      console.error('_isUserUniq',e);
    }
  }


  _genWallet = () => {
    var wallet = genWallet()
    this.setState({
      inpAddr: wallet.address,
      inpKey: wallet.privateKey
    })
  };


  _changeAvatar = async () => {
    const hash = await publicFileToIpfs()
    console.log('avatar added',hash);
    this.setState({avatar: hash})
    await AsyncStorage.setItem('@NknStore:avatar', hash);
  }


  _uploadKey = async () => {
    const file = await DocumentPicker.getDocumentAsync({});
    console.log('file', file);

    // TODO: file parse
    const key = {}

    this.setState({
      inpAddr: key.address,
      inpKey: key.privKey,
    })
  }


  _downloadKeyFile = () => {
    const keyJson = ethDownloadKeyFile();
    // TODO: save file
  }


  _changeAvatar = async () => {
    const url = `https://gateway.ipfs.io/ipfs/${await publicFileToIpfs()}`
    console.log('avatar added',url);
    this.setState({avatar: url})
    await AsyncStorage.setItem('@NknStore:avatar', url);
  }


  _help1 = () => {
    this.setState({
      inpName: 'Boris',
      inpAddr: '0xcd2e82ee657ec75aa3d7c296255ff1c75c0a9482',
      inpKey: '0x73E9DE34C6BEF463BD1B1AB621235FC780ED9B901FC423164C50C546991C94E5',
    })
  }

  _help2 = () => {
    this.setState({
      inpName: 'Boris2',
      inpAddr: '0x9641494bfb611b7348c10ee7f57805cf4aca0e09',
      inpKey: '0x55BDD9E183F6D2AE6FA296F1FA6102858A0C6E992EF47F2209F3FA87103244D6',
    })
  }


  componentDidUpdate = async () => {
    this._changeForm()
  }


  componentWillMount = async () => {
    const logged = await AsyncStorage.getItem('@NknStore:logged');
    const name = await AsyncStorage.getItem('@NknStore:name');
    const avatar = await AsyncStorage.getItem('@NknStore:avatar');
    const nknAddr = await AsyncStorage.getItem('@NknStore:nknAddr');
    const address = await AsyncStorage.getItem('@NknStore:ethAddress');
    const privateKey = await AsyncStorage.getItem('@NknStore:ethPrivateKey');
    const chatHistory = await AsyncStorage.getItem('@NknStore:chatHistory');
    const widgets = await AsyncStorage.getItem('@NknStore:widgets');
    const friends = await AsyncStorage.getItem('@NknStore:friends');

    this.setState({
      logged: logged,
      name: name,
      avatar: avatar,
      nknAddr: nknAddr,
      address: address,
      privateKey: privateKey,
      chatHistory: chatHistory,
      widgets: widgets,
      friends: friends,
    })

    if (logged === 'true') {
      const {name, nknAddr, avatar} = this.state
      const user = { name: name, addressNkn: nknAddr, avatar: avatar, desc: '' }
      this.props.navigation.navigate('User');
    }
  }


  componentDidMount = () => {
    // TODO: if not registered, generate new wallet, and give possibility to put name to contract from server wallet (without money on new wallet)
    // TODO: if not logged in, show address, hide privateKey
    // this._genWallet()
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  developmentModeText: {
    marginBottom: 20,
    color: '#2e78b7',
    fontSize: 16,
    lineHeight: 19,
    textAlign: 'center',
  },
  contentContainer: {
    paddingTop: 30,
  },
  welcomeContainer: {
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 20,
  },
  welcomeImage: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    marginTop: 3,
    marginLeft: -10,
  },
  getStartedContainer: {
    alignItems: 'center',
    marginHorizontal: 50,
  },
  homeScreenFilename: {
    marginVertical: 7,
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  codeHighlightContainer: {
    backgroundColor: 'rgba(0,0,0,0.05)',
    borderRadius: 3,
    paddingHorizontal: 4,
  },
  getStartedText: {
    fontSize: 17,
    color: 'rgba(96,100,109, 1)',
    lineHeight: 24,
    textAlign: 'center',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 10,
  },
  tabBarInfoText: {
    fontSize: 17,
    color: 'rgba(200,0,0, 1)',
    textAlign: 'center',
  },
  navigationFilename: {
    marginTop: 5,
  },
  helpContainer: {
    marginTop: 15,
    alignItems: 'center',
  },
  inputContainer: {
    width: '100%',
    alignItems: 'center',
  },
  helpLink: {
    paddingVertical: 10,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  labelText: {
    fontSize: 12,
    color: 'gray',
  },
  inputText: {
    alignItems: 'center',
    textAlign: 'left',
    height: 40,
    width: '80%',
    borderColor: 'gray',
    borderWidth: 0
  },
  inputName: {
    alignItems: 'center',
    textAlign: 'left',
    height: 40,
    width: '80%',
    borderColor: 'gray',
    borderWidth: 0
  },
  inputTextMultiline: {
    alignItems: 'center',
    textAlign: 'left',
    width: '80%',
    borderColor: 'gray',
    borderWidth: 0
  },
});
