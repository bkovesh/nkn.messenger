import React from 'react';
import { ScrollView, StyleSheet, View, TextInput, Text, FlatList, ListItem, TouchableOpacity, Picker, AsyncStorage } from 'react-native';
import { List, Button, Divider, Card, ButtonGroup } from 'react-native-elements';
import { Dropdown } from 'react-native-material-dropdown';
import { ExpoLinksView } from '@expo/samples';
import { nknConnect, nknSendMessage } from '../helpers/nknApi';
import { ethContractFunc, ethUploadFile, ethAbiParse } from '../helpers/ethApi';
import { abiRegistry } from '../contracts/abiRegistry';
import { REGISTRY_CONTRACT_ADDRESS } from 'react-native-dotenv'



export default class WidgetsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      textName: '',
      widgets: [],
      widgetConstructor: [],
      choosenWidget: 0,
      ethFuncList: [],
      selectedIndex: 0,
    };
  }

  static navigationOptions = {
    title: 'Widgets',
  };

  render() {

    let container
    if (this.state.selectedIndex === 0) {
      container = (
        <View style={styles.resultContainer}>
          <Text style={{}}>────────  Widget Constructor  ────────</Text>
          <TouchableOpacity onPress={this._ethDeployContract} style={styles.helpLink}>
            <Text style={styles.helpLinkText}>Upload new contract</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this._ethAbiParse} style={styles.helpLink}>
            <Text style={styles.helpLinkText}>Parse Abi</Text>
          </TouchableOpacity>
          <View style={styles.leftContainer}>
            {this.state.ethFuncList}
          </View>
        </View>
      )
    } else if (this.state.selectedIndex === 1) {
      container = (
        <View style={styles.resultContainer}>
          <Text style={{}}>────────  My Widgets  ────────</Text>
        </View>
      )
    } else {
      container = (
        <View style={styles.resultContainer}>
          <Text style={{}}>────────  Widget Store  ────────</Text>
        </View>
      )
    }

    return (
      <ScrollView style={styles.container}>

        <ButtonGroup
          onPress={(ind) => {
            this.setState({selectedIndex: ind})
          }}
          selectedIndex={this.state.selectedIndex}
          buttons={['Constructor','My Widgets', 'Widget Store']}
          containerStyle={{height: 30}}
          textStyle={{fontSize: 14}}
        />


        {container}




      </ScrollView>
    );
  }


  _ethDeployContract = () => {
    ethUploadFile()
    // TODO: add to my widgets
    // TODO: if solc wont work, user should deploy contract via remix, and deploy abi.json via this messenger to ipfs and add hash of it to registry
  }


  _searchWidgetByName = async (name) => {
    const foundWidget = await ethContractFunc(ethPrivateKey, REGISTRY_CONTRACT_ADDRESS, false, 'showWidgetByName', [name])
  }


  _showMyWidgets = async () => {
    const ethPrivateKey = await AsyncStorage.getItem('@NknStore:ethPrivateKey');
    const address = await AsyncStorage.getItem('@NknStore:ethAddress');
    // TODO: write function in contract to find my widgets and use it here
    const foundWidgets = await ethContractFunc(ethPrivateKey, REGISTRY_CONTRACT_ADDRESS, false, 'showWidgetsOfOwner', [address])

    // const myWidgets = await AsyncStorage.getItem('@NknStore:widgets')
    return myWidgets;
  }


  _ethAbiParse = () => {
    // TODO: add abi from widget, choosen in list
    const abi = abiRegistry
    const list = [];
    let i = 0;
    abi.forEach((obj)=>{
      const { funcName, choosenWidget } = this.state
      const args = []
      let i2 = 0;

      obj.inputs.forEach((obj)=>{
        // TODO: when args type is address, you set, whome address it is, client or owner
        // TODO: if args type is uint, you set, is it textinput, or date input
        args.push(
          <View style={{flexDirection: 'row'}} key={i}>
            <Text style={{flex:1,fontSize: 12}}>{obj.name}</Text>
            <TextInput
              style={{flex:3,width:'100%',height:40}}
              multiline = {false}
              onChangeText={(val) => {
                let wc = this.state.widgetConstructor
                wc.push({})
                wc[choosenWidget][funcName] = {}
                wc[choosenWidget][funcName]['args'] = {}
                wc[choosenWidget][funcName]['args'].value = val
                this.setState({widgetConstructor: wc})
              }}
              value={this.state.funcName}
            />
            <Dropdown
              containerStyle={{flex:2}}
              label='Type'
              data={[
                { value: 'text' },
                { value: 'time' },
              ]}
              fontSize={12}
              labelFontSize={10}
              onChangeText={(val,i,data) => {
                let wc = this.state.widgetConstructor
                wc.push({})
                wc[choosenWidget][funcName] = {}
                wc[choosenWidget][funcName]['args'] = {}
                wc[choosenWidget][funcName]['args'].type = val
                this.setState({widgetConstructor: wc})
              }}
            />
          </View>
        )
        i2++
      })

      list.push(
        <View style={{marginTop:15}} key={i}>
          <View style={{alignItems:'center', backgroundColor: 'rgba(0,0,0,0.05)'}}>
            <Text style={{fontWeight:'bold'}}>{obj.name}</Text>
          </View>
          <View style={{}}>

            <Text style={{fontSize: 12, color:'gray'}}>Function name</Text>
            <TextInput
              style={{width:'100%'}}
              multiline = {false}
              onChangeText={(val) => {
                let wc = this.state.widgetConstructor
                wc.push({})
                wc[choosenWidget][funcName] = {}
                wc[choosenWidget][funcName].name = val
                this.setState({widgetConstructor: wc})
              }}
              value={this.state.funcName}
            />


            <Dropdown
              label='Visibility'
              data={[
                { value: 'me' },
                { value: 'others' },
                { value: 'both' }
              ]}
              fontSize={12}
              labelFontSize={10}
              onChangeText={(val,i,data) => {
                let wc = this.state.widgetConstructor
                wc.push({})
                wc[choosenWidget][funcName] = {}
                wc[choosenWidget][funcName].visibility = val
                this.setState({widgetConstructor: wc})
              }}
            />

            <Dropdown
              label='Place'
              data={[
                { value: 'chat' },
                { value: 'page' },
              ]}
              fontSize={12}
              labelFontSize={10}
              onChangeText={(val,i,data) => {
                let wc = this.state.widgetConstructor
                wc.push({})
                wc[choosenWidget][funcName] = {}
                wc[choosenWidget][funcName].place = val
                this.setState({widgetConstructor: wc})
              }}
            />

          <Text style={{fontSize: 12,backgroundColor:'rgba(0,0,0,0.05)',textAlign:'center'}}>Function arguments</Text>
            {args}
          </View>
        </View>
      )
      i++
    })
    this.setState({ethFuncList:list})
  }


  componentWillMount = async () => {
    // TODO: show my widgets
    // TODO: show all widgets
  }


}


//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  resultContainer: {
    marginTop: 10,
    alignItems: 'center',
  },
  leftContainer: {
    marginTop: 10,
    textAlign: 'left',
    width: '90%',
  },
  helpLink: {
    paddingVertical: 10,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
  leftText: {
    textAlign: 'left',
    fontSize: 14,
  },
});
