import React, {Component} from 'react';
import { ScrollView, StyleSheet, View, TextInput, Text, FlatList, ListItem, TouchableOpacity, Image, AsyncStorage, Linking, Platform } from 'react-native';
import { Button, ButtonGroup } from 'react-native-elements';
import { publicFileToIpfs } from '../helpers/ipfsApi';
import { Icon } from 'expo';
import Colors from '../constants/Colors';

export default class UserScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedIndex: 0,
      name: 'User Name',
      avatar: 'QmdAVdyJYV8DYrG13pZUvf8ZZah3btk8VfE4iVbbegoqtF',
      desc: 'My name is User Name. I am from ... I think, that crypto-sphere has a big future.'
    };
  }


  static navigationOptions = {
    title: 'User',
  };


  render() {
    const { navigation } = this.props;
    const name = navigation.getParam('name', 'name');
    const avatar = navigation.getParam('avatar', 'avatar');
    const desc = navigation.getParam('desc', 'desc');


    let container
    if (this.state.selectedIndex === 0) {
      container = (
        <View style={{width: '90%', marginBottom: 10, alignItems: 'center',}}>
          <Text style={{}}>────────  About  ────────</Text>
          <Text style={{}}>{desc}</Text>
        </View>
      )
    } else {
      container = (
        <View style={{width: '90%', marginBottom: 10, alignItems: 'center',}}>
          <Text style={{}}>────────  Widgets  ────────</Text>
          <Text style={{}}>User's Widget List</Text>
        </View>
      )
    }


    return (
      <ScrollView style={styles.container}>
        <View style={styles.resultContainer}>

          {/* Avatar */ }
          <TouchableOpacity style={{marginBottom: 10}} onPress={this._changeAvatar}>
            <Image style={styles.avatar} source={{uri: 'https://gateway.ipfs.io/ipfs/'+avatar}}/>
          </TouchableOpacity>
          <Text style={{fontWeight: 'bold',margin:10}}>{name}</Text>

          <View style={{flexDirection: 'row'}}>
            <Button
              style={{}}
              title='Add'
              fontSize={12}
              buttonStyle={{
                backgroundColor: Colors.tintColor,
                width: 60,
                height: 30,
                borderColor: "transparent",
                borderWidth: 0,
                borderRadius: 5
              }}
              onPress={this._changeAvatar}
            />
            <Button
              title='Chat'
              fontSize={12}
              buttonStyle={{
                backgroundColor: Colors.tintColor,
                width: 60,
                height: 30,
                borderColor: "transparent",
                borderWidth: 0,
                borderRadius: 5
              }}
              onPress={this._goToChat}
            />
          </View>

          <ButtonGroup
            onPress={(ind) => {
              this.setState({selectedIndex: ind})
            }}
            selectedIndex={this.state.selectedIndex}
            buttons={['About','Widgets']}
            containerStyle={{height: 30}}
            textStyle={{fontSize: 14}}
          />

          {container}

        </View>
      </ScrollView>
    );
  }


  _goToChat = () => {
    const { navigation } = this.props;
    const name = navigation.getParam('name', 'name');
    const avatar = navigation.getParam('avatar', 'avatar');
    const desc = navigation.getParam('desc', 'desc');
    const addressNkn = navigation.getParam('addressNkn', '');
    console.log('_goToChat',addressNkn);

    const user = { name: name, addressNkn: addressNkn, avatar: avatar, desc: desc }
    this.props.navigation.navigate('Chat', user)
  }


  _addUserToFriends = async () => {
    const { name, nknAddr, avatar, desc } = this.state
    const friendObj = { name: name, addressNkn: nknAddr, avatar: avatar, desc: desc }
    const friends = await AsyncStorage.getItem('@NknStore:friends');
    let friendsJson = JSON.parse(friends)
    friends ? friendsJson.push(friendObj) : friendsJson = [friendObj]
    const friendsStr = JSON.stringify(friendsJson)
    this.setState({friends: friendsJson})
    return await AsyncStorage.setItem('@NknStore:friends', friendsStr);
  }


  _loadAvatar = async () => {
    const url = await AsyncStorage.getItem('@NknStore:avatar')
    this.setState({avatar: url})
    return url
  }


  _changeAvatar = async () => {
    const url = `https://gateway.ipfs.io/ipfs/${await publicFileToIpfs()}`
    console.log('avatar added',url);
    this.setState({avatar: url})
    await AsyncStorage.setItem('@NknStore:avatar', url);
  }


  componentWillMount = async () => {
    console.log('User will mount');
    const name = await AsyncStorage.getItem('@NknStore:lookAtUser');
    if (name === '') {
      const myName = await AsyncStorage.getItem('@NknStore:name');
      this.setState({name:myName})
    } else {
      this.setState({name:name})
    }
    this._loadAvatar()
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
    backgroundColor: '#fff',
  },
  resultContainer: {
    marginTop: 0,
    alignItems: 'center',
  },
  avatar: {
    width: 70,
    height: 70,
    borderRadius: 35,
    borderColor: 'rgba(0,0,0,0.1)',
    borderWidth: 1,
    padding: 10,
    marginBottom: 0,
  },
});
