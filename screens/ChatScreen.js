import React from 'react';
import { View, TextInput, Text, FlatList, ListItem,
  Image, Platform, ScrollView, StyleSheet, TouchableOpacity, Button, AsyncStorage, KeyboardAvoidingView
} from 'react-native';
import { DocumentPicker, ImagePicker } from 'expo';
import { ExpoLinksView } from '@expo/samples';
import { nknSendMessage } from '../helpers/nknApi';
import { publicFileToIpfs, publicJsonToIpfs, getJsonFromIpfs } from '../helpers/ipfsApi';
import nkn from 'nkn-client'
let NKN_CLIENT = {}
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////


export default class ChatScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inpText: '',
      msgReceiver: '',
      nknAddr: '',
      refresh: false,
      widgets: [],
      chatHistory: [],
    };
    this._nknSendMessage = this._nknSendMessage.bind(this);
  }

  static navigationOptions = {
    title: 'Chat',
  };



  render() {
    const { chatHistory, msgReceiver } = this.state


    return (
      <View style={styles.container}>
        <ScrollView style={styles.container2} contentContainerStyle={styles.contentContainer}>
          <FlatList
            extraData={chatHistory}
            data={chatHistory}
            renderItem={({item}) =>
              item[msgReceiver].pair[1] === msgReceiver ?
              <Text style={styles.textMe}>{item[msgReceiver].text}</Text> :
              <Text style={styles.textNotMe}>{item[msgReceiver].text}</Text>
            }
          />
        </ScrollView>

        <KeyboardAvoidingView style={styles.aboveKeyboardContainer} keyboardVerticalOffset={80} behavior="padding" enabled>
          <View style={styles.box1}>
            <TextInput
              style={styles.inputTextGrowing}
              multiline = {true}
              onChangeText={(inpText) => this.setState({inpText})}
              value={this.state.inpText}
              autoFocus={true}
              autoGrow={true}
              maxHeight = {120}
              minHeight = {40}
              placeholderTextColor="grey"
              placeholder="Message"
              underlineColorAndroid="#fbfbfb"
            />
          </View>
          <View style={styles.box2}>
            <TouchableOpacity onPress={this._nknSendMessage} style={styles.helpLink}>
              <Text style={styles.helpLinkText}>Send</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={this._nknChooseWidget} style={styles.helpLink}>
              <Text style={styles.helpLinkText}>Widget</Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>

      </View>
    );
  }


  /*
  _onReceiveMessage = async () => {
    // TODO: listen to friend to receive messages, and update chat (setInterval or WS)
    NKN_CLIENT.on('message', (src, payload, payloadType) => {
      if (payloadType === nkn.PayloadType.TEXT) {
        console.log('Receive text message:', src, payload);
      } else if (payloadType === nkn.PayloadType.BINARY) {
        console.log('Receive binary message:', src, payload);
      }
    });
  }
  */


  _nknSendMessage = async () => {
    try {
      const {inpText, nknAddr} = this.state;

      const ipfsChatHistory = await AsyncStorage.getItem('@NknStore:chatHistory');
      const chatHistory = await getJsonFromIpfs(ipfsChatHistory);
      const newChatHistory = {}
      if (chatHistory) {
        newChatHistory = chatHistory;
      }

      const { navigation } = this.props;
      const msgReceiver = navigation.getParam('addressNkn', '');
      console.log('msgReceiver',msgReceiver);
      const msg = {
        key: 'name',
        pair: [nknAddr, msgReceiver],
        time: Date.now(),
        text: inpText,
        file: { hash: '' },
        widget: { address: '' },
        widgetEvent: { address: '', event: '' },
      }
      NKN_CLIENT.send(msgReceiver,JSON.stringify(msg));

      if (!newChatHistory[msgReceiver]) { newChatHistory[msgReceiver] = [] };
      newChatHistory[msgReceiver].push(msg);
      const ipfsNewChatHistory = await publicJsonToIpfs(newChatHistory);
      this.setState({
        chatHistory: newChatHistory,
      })
      await AsyncStorage.setItem('@NknStore:chatHistory',ipfsNewChatHistory);

    } catch (e) {
      console.error('_nknSendMessage',e);
    }
  };


  _nknChooseWidget = async () => {
    const {inpText, chatHistory, msgReceiver, nknAddr} = this.state;
    // TODO: render window with user's widgets
    const widgets = await AsyncStorage.getItem('@NknStore:widgets');
  }


  // TODO: use sendMessage
  _nknSendWidget = async () => {
    // TODO: user can open accordion in chat and fill the form of widget
    // TODO: delete from msg all unused keys
    const msg = {
      key: name,
      pair: [nknAddr, msgReceiver],
      time: Date.now(),
      text: inpText,
      file: { hash: '' },
      widget: { address: '' },
      widgetEvent: { address: '', event: '' },
    }
  }


  componentWillMount = async () => {
    // TODO: connect to nkn on loading app
    try {
      console.log('chat will mount');
      const logged = await AsyncStorage.getItem('@NknStore:logged');
      const name = await AsyncStorage.getItem('@NknStore:name');
      const nknAddr = await AsyncStorage.getItem('@NknStore:nknAddr');
      const address = await AsyncStorage.getItem('@NknStore:ethAddress');
      const privateKey = await AsyncStorage.getItem('@NknStore:ethPrivateKey');
      const ipfsChatHistory = await AsyncStorage.getItem('@NknStore:chatHistory');
      const widgets = await AsyncStorage.getItem('@NknStore:widgets');
      const friends = await AsyncStorage.getItem('@NknStore:friends');

      this.setState({
        logged: Boolean(logged),
        name: name,
        nknAddr: nknAddr,
        address: address,
        privateKey: privateKey,
        chatHistory: await getJsonFromIpfs(ipfsChatHistory),
        widgets: widgets,
        friends: friends,
      })

      const { navigation } = this.props;
      const msgReceiver = navigation.getParam('addressNkn', '');
      console.log('msgReceiver',msgReceiver);

      const chatHistory = await getJsonFromIpfs(ipfsChatHistory);
      const newChatHistory = {}
      if (chatHistory) {
        newChatHistory = chatHistory;
      }

      NKN_CLIENT = await nkn({
        identifier: address,
        privateKey: privateKey,
        seedRpcServerAddr: 'http://104.196.247.255:30003',
      });
      if (logged === 'true') {
        NKN_CLIENT.on('connect', () => {
          console.log('Connection opened.');
          NKN_CLIENT.on('message', async (sender, payload, payloadType) => {
            console.log('Message received',payload);
            if (!newChatHistory[sender]) { newChatHistory[sender] = [] };
            newChatHistory[sender].push(payload);
            const ipfsNewChatHistory = await publicJsonToIpfs(newChatHistory);
            this.setState({
              chatHistory: newChatHistory,
            })
            await AsyncStorage.setItem('@NknStore:chatHistory',ipfsNewChatHistory);
          });
        });
      }
    } catch (e) {
      console.error('Chat',e);
    }
  }



}


//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////


const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 0,
    backgroundColor: '#fff',
  },
  container2: {
    flex: 20,
    marginTop: 0,
    paddingTop: 0,
    paddingBottom: 10,
    backgroundColor: '#fff',
  },
  box1: {
    flex: 6,
    alignItems: 'center',
  },
  box2: {
    flex: 1,
    alignItems: 'center',
  },
  contentContainer: {
    paddingTop: 0,
  },
  resultContainer: {
    marginTop: 30,
    // alignItems: 'center',
  },
  textNotMe: {
    borderRadius: 10,
    padding: 10,
    margin: 5,
    backgroundColor: 'rgba(220,200,100, 0.3)',
    marginRight: '30%',
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 1,
      },
    })
  },
  textMe: {
    borderRadius: 10,
    padding: 10,
    margin: 5,
    backgroundColor: 'rgba(0,220,0, 0.1)',
    marginLeft: '30%',
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 1,
      },
    })
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 10,
  },
  aboveKeyboardContainer: {
    flex: 0.2,
    flexDirection: 'row',
    padding: 10,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
  },
  tabBarInfoText: {
    fontSize: 17,
    textAlign: 'center',
  },
  inputText: {
    alignItems: 'center',
    textAlign: 'left',
    height: 40,
    width: '100%',
    borderColor: 'gray',
    borderWidth: 0
  },
  inputTextGrowing: {
    alignItems: 'center',
    textAlign: 'left',
    width: '100%',
  },
  helpLink: {
    paddingVertical: 10,
  },
  helpLinkText: {
    fontSize: 14,
    color: '#2e78b7',
  },
});
