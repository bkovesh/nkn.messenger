import nkn from 'nkn-client'



export const nknConnect = (ethAddress,ethPrivateKey) => {
  var client = nkn({
    identifier: ethAddress,
    privateKey: ethPrivateKey,
    // seedRpcServerAddr: 'http://35.197.90.102:30003',
    // responseTimeout: 5,
  });
  console.log(client.addr);

  client.on('connect', () => {
    console.log('Connected');
  })

  client.on('message', (src, payload, payloadType) => {
    if (payloadType === nkn.PayloadType.TEXT) {
      console.log('Receive text message:', src, payload);
    } else if (payloadType === nkn.PayloadType.BINARY) {
      console.log('Receive binary message:', src, payload);
    }
  });

  return client;
}



export const nknSendMessage = (client, addr, text) => {
  const address = addr + ".0391d29bcf426050586739b55e7f3f774e812aaf1378142403def0457deeba9a13"
  client.send(address, text)
}
