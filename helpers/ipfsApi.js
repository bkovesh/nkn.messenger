import { AsyncStorage } from 'react-native';
import { DocumentPicker, ImagePicker } from 'expo';





export const publicJsonToIpfs = async (json) => {
  const data = new FormData();
  data.append('name', JSON.stringify(json));

  const url = 'https://ipfs.infura.io:5001/api/v0/add?pin=false&quieter=true';
  return await fetch(url, {
    method: 'POST',
    header: {
     "Content-Type": "multipart/form-data"
    },
    body: data
  }).then(res => {
    return res.json()
  }).then(res => {
    return res.Hash
  }).catch(err => {
    return err
    console.error(err);
  });
}




export const publicFileToIpfs = async () => {
  const file = await DocumentPicker.getDocumentAsync({});
  console.log('file', file);

  const data = new FormData();
  // data.append('name', JSON.stringify([{name:'testName'}]));
  data.append('file', {
    name: file.name,
    uri: file.uri,
    type: 'image/jpeg',
  });

  const url = 'https://ipfs.infura.io:5001/api/v0/add?pin=false&quieter=true';
  return await fetch(url, {
    method: 'POST',
    header: {
     "Content-Type": "multipart/form-data"
    },
    body: data
  }).then(res => {
    return res.json()
  }).then(res => {
    return res.Hash
  }).catch(err => {
    console.error(err);
  });
}



export const getJsonFromIpfs = async (hash) => {

  const url = 'https://ipfs.infura.io:5001/api/v0/cat?arg='+hash;
  return await fetch(url, {
    method: 'GET',
    header: {
     "Content-Type": "multipart/form-data"
    },
    // body: data,
  }).then(res => {
    return res.json()
  }).then(res => {
    return res
  }).catch(err => {
    console.error(err);
  });
}






/*
// import ipfsAPI from 'ipfs-api';
// import IPFS from 'ipfs';
// import PubSubRoom from 'ipfs-pubsub-room';
// const ipfs = new IPFS({ repo: 'ipfs-' + Math.random() });
// const ipfs = new IPFS({
//   EXPERIMENTAL: {
//     pubsub: true
//   },
//   config: {
//     Addresses: {
//       Swarm: [
//         '/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star'
//       ]
//     }
//   }
// })
// var ipfs = ipfsAPI('/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star')


export const roomMsg = (newMsg) => {

  // IPFS node is ready, so we can start using ipfs-pubsub-room
  // ipfs.on('ready', () => {
    // const room = PubSubRoom(ipfs, 'room-name')
    //
    // room.on('peer joined', (peer) => {
    //   console.log('Peer joined the room', peer)
    // })
    //
    // room.on('peer left', (peer) => {
    //   console.log('Peer left...', peer)
    // })
    //
    // // now started to listen to room
    // room.on('subscribed', () => {
    //   console.log('Now connected!')
    // })
  // })
}




// опубликовать в ИПФС и добавить в блокчейн
export const ipfsDraft = () => {
    // const ipfs = new window.Ipfs({ repo: 'ipfs-' + Math.random() })
    ipfs.once('ready', () => {
        console.log('Online status: ', ipfs.isOnline() ? 'online' : 'offline')
        // ipfs.files.add(new ipfs.types.Buffer(file), (err, filesAdded) => {
        //     if (err) {
        //         return console.error('Error - ipfs files add', err, filesAdded)
        //     }
        //     filesAdded.forEach((file) => {
        //       console.log(file.hash);
        //     })
        // })
    })
}
*/


// функция отправки сообщения
// export const newMessage = async (newMsg) => {
  /**
  // NOTE:
  * общий объект хранится в ИПФС непонятно для чего
  * сначала история сообщений записывается в локале, потом при наступлении какого-нибудь события отправляется в сеть
  */
  // !!! убрать загрузку общего объекта в ипфс, хранить хэши в локале
  // берем старый общий объект с хэшами из локале
  // const messagesHash = await AsyncStorage.getItem('@NknStore:messages');
  // из него берем по хэшам объект переписки
  // const messagesObj = loadFromIpfs(messagesHash)
  // добавляем новое сообщение и заливаем обновленный объект в сеть
  // messagesObj.push({})
  // messagesHash = publicToIpfs(messagesObj);
  // записываем хэш сообщений в локале
  // await AsyncStorage.setItem('@NknStore:messages', messagesHash);
// }


/*
// опубликовать в ИПФС и добавить в блокчейн
export const publicToIpfs = (file) => {
    // const ipfs = new window.Ipfs({ repo: 'ipfs-' + Math.random() })
    ipfs.once('ready', () => {
        console.log('Online status: ', ipfs.isOnline() ? 'online' : 'offline')
        ipfs.files.add(new ipfs.types.Buffer(file), (err, filesAdded) => {
            if (err) {
                return console.error('Error - ipfs files add', err, filesAdded)
            }
            filesAdded.forEach((file) => {
              console.log(file.hash);
            })
        })
    })
}


// upload net as a json file
export const loadFromIpfs = (q) => {
  return fetch("http://gateway.ipfs.io/ipfs/"+q)
    .then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
      return responseJson;
    })
    .catch((error) => {
      console.error(error);
    });
}
*/
