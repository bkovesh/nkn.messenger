////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//// Objects


var message = {
  pair: ['addressNknFrom', 'addressNknTo'],
  time: 122432,
  text: 'text',
  file: { hash: 'hashIpfs' },
  widget: { address: 'addressEth' },
  widgetEvent: { address: 'addressEth', event: 'text' },
}


var widget = {
  name: 'text',
  avatar: 'hashIpfs',
  description: 'text',
  category: 'text',
  address: 'addressEth',
  abi: 'hashIpfs',
  sol: 'hashIpfs',
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//// LocalStorage


var userLocal = {
  name: 'text',
  avatar: 'hashIpfs',
  description: 'text',

  addressNkn: 'addressNkn',
  addressEth: 'addressEth',
  privateKeyEth: 'hashEth',

  friends: friends,
  widgets: widgets,
}



var friends = [
  { name: 'text', addressNkn: 'addressNkn', chatHistory: 'hashIpfs', avatar: 'hashIpfs' },
  { name: 'text', addressNkn: 'addressNkn', chatHistory: 'hashIpfs', avatar: 'hashIpfs' },
]



var widgets = [
  { name: 'text', addressEth: 'addressNkn', abi: 'hashIpfs' },
  { name: 'text', addressEth: 'addressNkn', abi: 'hashIpfs' },
]


// NOTE: load chatHistory1,2.. from ipfs
// NOTE: chatHistory1,2... cuts by days
var chatHistory = [
  chatHistory1,
  chatHistory2,
  chatHistory3,
]


////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//// IPFS



var userPublic = {
  name: 'text',
  addressNkn: 'addressNkn',
  addressEth: 'addressEth',
  avatar: 'hashIpfs',
  description: 'text',
}



var widget = {
  name: 'text',
  avatar: 'hashIpfs',
  description: 'text',
  category: 'text',
  address: 'addressEth',
  abi: 'hashIpfs',
  functions: [
    {
      nameInContract: 'textAbi',
      name: 'text',
      description: 'text',
      visible: 'me/friend/everybody',
      place: 'chat/userpage/both', // private/public
      args: [{ nameInContract: 'textAbi', name: 'text', type: 'string/number/date' }],
      returns: [{ nameInContract: 'textAbi', name: 'text', type: 'string/number/date/obj' }],
      events: [
        {
          nameInContract: 'text',
          name: 'rus text',
          args: [{ nameInContract: 'textAbi', name: 'text', type: 'string/number/date' }]
        }
      ]
    }
  ]
}



var chatHistory = [
  message,
  message,
  eventFromWidget,
]

  // { key: 1, time: 1537631683100, name: 'me', text: 'Hi!!! How are you?' },
  // { key: 2, time: 1537631683108, name: 'not me', text: 'Hi! I am fine! You?' },
  // { key: 3, time: 1537631683118, name: 'me', text: 'I too, thanks!' },
