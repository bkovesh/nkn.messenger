import ethers from 'ethers';
import { DocumentPicker, ImagePicker, FileSystem } from 'expo';
const path = require('path');
// const fs = require('fs');
var Wallet = ethers.Wallet;
// import solcjs from 'solc-js';
import {providers} from 'ethers';
import {abiRegistry} from '../contracts/abiRegistry';
const network = providers.networks.rinkeby;
const infuraProvider = new providers.InfuraProvider(network);


export const genWallet = () => {
  var wallet = Wallet.createRandom();
  return wallet
}


export const compileContractInBrowser = () => {
  var res = window.BrowserSolc.loadVersion("soljson-v0.4.18+commit.9cf6e910.js", (compiler) => {
    let source = 'contract x { function g() {} }';
    let optimize = 1;
    let result = compiler.compile(source, optimize);
    console.log('result', result);
    return result
  });
}


export const compileContractInBrowserAsync = async () => {
  var res = window.BrowserSolc.loadVersion("soljson-v0.4.18+commit.9cf6e910.js", (compiler) => {
    let source = 'contract x { function g() {} }';
    let optimize = 1;
    let result = compiler.compile(source, optimize);
    console.log('result', result);
    return result
  });
  return await res
}


export const compileContractInBrowserPromise = () => {
  // const contractPath = path.resolve(__dirname, '', '../contracts/index.sol');
  // const source = fs.readFileSync(contractPath, 'UTF-8');

  var promise = new Promise(() => window.BrowserSolc.loadVersion("soljson-v0.4.18+commit.9cf6e910.js"))
  console.log('promise', promise);
  var source = 'contract x { function g() {} }';
  var optimize = 1;
  var result = promise.then(compiler => {
    var res = compiler.compile(source, optimize)
    return res
  })
  console.log('result', result);
  return result
}


// does not work
export const compileContract = () => {
  // const source = 'contract MyContract { function g() {} }';
  // console.log(solc);
  // const contractInfo = solc.compile(source, 1).contracts[':MyContract'];
  // console.log(contractInfo.interface);
  solc.loadVersion("soljson-v0.4.6+commit.2dabbdf0.js", function(compiler) {
    let source = 'contract x { function g() {} }';
    let optimize = 1;
    let result = compiler.compile(source, optimize);
    console.log(result);
  });
}


// !!! если solc не работает, осставить так, ччтобы можно было вручную вводить адрес и загружать файл с аби
export const ethUploadFile = async () => {
  const file = await DocumentPicker.getDocumentAsync({});
  const fileStr = await FileSystem.readAsStringAsync(file.uri);
  // console.log('fileStr', fileStr);
  // console.log(solcjs);

  /*
  const compiled = solcjs.loadVersion("soljson-v0.4.18+commit.9cf6e910.js", (compiler) => {
    let source = fileStr;
    let optimize = 1;
    let result = compiler.compile(source, optimize);
    console.log('gsdfg');
  })
  */

}



export const deployContract = () => {
  var contractInfo = {}
  compileContractInBrowserPromise().then((cc) => {
    console.log('contractInfo', cc);
  })
  // const YOUR_PRIVATE_KEY = '0x42FAB52A2441FB88D663FB92E79E6345EF72278EDA304B43A2D26C7C19CE8BC1'
  // const network = providers.networks.rinkeby;
  // const infuraProvider = new providers.InfuraProvider(network);
  // const wallet = new ethers.Wallet(YOUR_PRIVATE_KEY, infuraProvider);
  //
  // const deployTransaction = ethers.Contract.getDeployTransaction('0x'+contractInfo.bytecode, contractInfo.interface);
  // console.log(deployTransaction);
  //
  // const sendPromise = wallet.sendTransaction(deployTransaction);
  // sendPromise.then(function(transaction) {
  //     console.log(transaction);
  // });
}


// вызов функции контракта
export const ethContractFunc = async (privateKey, contractAddress, write, funcName, args) => {
  // console.log('ethContractFunc');
  const wallet = new ethers.Wallet(privateKey, infuraProvider);
  var contract = new ethers.Contract(contractAddress, abiRegistry, wallet);
  var txCount = await infuraProvider.getTransactionCount(wallet.address, 'pending');

  var options = {}
  if (write) {
    options = {
        gasLimit: 5000000,
        gasPrice: 9000000000,
        nonce: txCount,
        value: ethers.utils.parseEther('0')
    };
  }

  // console.log(txCount)
  return contract.functions[funcName](...args, options)
  .then((res) => {
    console.log('ethContractFunc:',res);
    return res
  })
  .catch((err) => {
    return err
    console.log('ethContractFunc:',err)
  });
}



// составление списка функций из аби контракта
export const ethAbiParse = (abi) => {
  const arr = []
  for (var i = 0; i < abi.length; i++) {
    arr[i] = abi[i].name
    console.log(arr[i]);
  }
  return arr
}


// составление файла ключа типа MyEtherWallet
export const ethDownloadKeyFile = (privateKey) => {
  const wallet = new ethers.Wallet(privateKey, infuraProvider);
  var key = {
    "address": wallet.address.toLowerCase(),
    "checksumAddress": wallet.address,
    "privKey": wallet.privateKey.slice(2),
    "pubKey": new ethers.SigningKey(wallet.privateKey).publicKey,
    "publisher":"MyEtherWallet",
    "encrypted":false,
    "version":2
  }
  return key;
}
