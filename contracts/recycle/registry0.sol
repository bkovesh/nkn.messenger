pragma experimental ABIEncoderV2;
/*
NOTE:
* администратор не может изменить пользователя



*/



contract Main {

  // users
  mapping (address => uint) indexOfUser;
  mapping (address => User) usersByAddr;
  mapping (string => User) usersByName;
  struct User {
    address owner;
    string name;
    string avatar;
    string status;
  }
  User[] users;

  // widgets
  mapping (address => Widget) widgetsByAddr;
  mapping (address => Widget) widgetsByOwner;
  mapping (string => Widget) widgetsByName;
  struct Widget {
    address owner;
    string name;
    string avatar;
    string description;
    address contractAddress;
  }
  Widget[] widgets;



  //// Административные функции ////

  //// управление пользователями

  function addUser(string name, string url, string status) public {
      users.push(User(msg.sender, name, url, status));
      usersByAddr[msg.sender] = users[users.length-1];
      usersByName[name] = users[users.length-1];
  }

  // TODO: give possibility to add user by admin
  function changeUser(string name, string url, string status) public {
      usersByAddr[msg.sender] = User(msg.sender, name, url, status);
  }

  // TODO: убрать индекс
  function delUser(uint id) public {
      require(msg.sender == usersByAddr[msg.sender].owner); //
      delete users[id]; // !!! изменяет индексы?
  }

  function showUser(uint id) view public returns(User) {
      return users[id];
  }

  function showUserByName(string name) view public returns(User) {
      return usersByName[name];
  }

  function isUserUniq(string name) view public returns(User) {
      return usersByName[name];
  }


  //// управление виджетами

  function addWidget(string name, string avatar, string desc, address contractAddr) public {
      widgets.push(Widget(msg.sender, name, avatar, desc, contractAddr));
  }

  function delWidget(uint id) public {
      require(msg.sender == widgets[id].owner);
      delete widgets[id]; // !!! изменяет индексы?
  }

  function changeWidget(string name, string avatar, string desc, address contractAddr) public {
      widgetsByAddr[msg.sender] = Widget(msg.sender, name, avatar, desc, contractAddr);
  }

  function showWidget(uint id) view public returns(Widget) {
      return widgets[id];
  }



}
