pragma solidity ^0.4.21;
/**
NOTE:
* контракт выставления счета (не безопасный)

*/



contract InvoiceContract {
  // Боб отправляет Алисе запрос с количеством денег, которрые она должна оплатить
  // Алиса видит запрос, количество денег и просто нажимает кнопку оплаты

  mapping (address => mapping (address => Invoice[])) invoices;

  struct Invoice {
    uint id;
    address sender;
    address receiver;
    uint amount; // in eth
    string status;
  }

  event changeStatus(address sender, address receiver, uint id, string status);


  //// Функции ////

  function addInvoice(address receiver, uint amount) public {
      uint len = invoices[msg.sender][receiver].length;
      invoices[msg.sender][receiver].push(Invoice(len, msg.sender, receiver, amount, 'created'));
      changeStatus(msg.sender, receiver, len, 'created');
  }

  function completeInvoice(address sender, uint id) public {
      Invoice storage inv = invoices[sender][msg.sender][id];
      require(inv.receiver == msg.sender);
      inv.receiver.transfer(inv.amount);
      invoices[sender][msg.sender][id] = Invoice(id, sender, msg.sender, inv.amount, 'done');
  }

  function delInvoice(address receiver, uint id) public {
      delete invoices[msg.sender][receiver][id]; // !!! изменяет индексы?
  }





}
