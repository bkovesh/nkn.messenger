pragma experimental ABIEncoderV2;
/*
NOTE:
* администратор не может изменить пользователя

*/



contract Registry {


  struct User {
    string name;
    address ethAddr;
    string nknAddr;
    string avatar;
    string desc;
  }
  mapping (string => User) usersByName;


  function addUser(string name, address ethAddr, string nknAddr, string avatar, string desc) public {
    require(usersByName[name].ethAddr == address(0)); // does not allow to rewrite address to name
    usersByName[name] = User(name, ethAddr, nknAddr, avatar, desc);
  }


  function changeUser(string name, address ethAddr, string nknAddr, string avatar, string desc) public {
    require(isUsernameUniq(name));
    usersByName[name] = User(name, ethAddr, nknAddr, avatar, desc);
  }


  function delUser(string name) public {
    require(msg.sender == usersByName[name].ethAddr); // TODO: add admin
    delete usersByName[name];
  }


  function showUserByName(string name) view public returns(User) {
    return usersByName[name];
  }


  function isUsernameUniq(string name) view public returns(bool) {
    return usersByName[name].ethAddr == address(0);
  }


  ////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////
  //// Widgets

  struct Widget {
    address owner;
    string name;
    string avatar;
    string description;
    address contractAddr;
    string ifpsSol;
    string ifpsAbi;
  }
  Widget[] widgets;
  mapping (address => Widget[]) widgetsOfOwner;
  mapping (string => Widget) widgetsByName;


  function addWidget(
    string name,
    string avatar,
    string desc,
    address contractAddr,
    string ifpsAbi,
    string ifpsSol
  ) public {
    Widget memory W = Widget(msg.sender, name, avatar, desc, contractAddr, ifpsAbi, ifpsSol);
    widgets.push(W);
    widgetsOfOwner[msg.sender].push(W);
    widgetsByName[name] = W;
  }


  function delWidget(string name) public {
    require(msg.sender == widgetsByName[name].owner); // TODO: add admin
    delete widgetsByName[name];
  }


  function changeWidget(
    string name,
    string avatar,
    string desc,
    address contractAddr,
    string ifpsAbi,
    string ifpsSol
  ) public {
    require(msg.sender == widgetsByName[name].owner); // TODO: add admin
    widgetsByName[name] = Widget(msg.sender, name, avatar, desc, contractAddr, ifpsAbi, ifpsSol);
  }


  function showWidgetsOfOwner(address owner) view public returns(Widget[]) {
    return widgetsOfOwner[owner];
  }


  function showWidgetByName(string name) view public returns(Widget) {
    return widgetsByName[name];
  }


  function isWidgetNameUniq(string name) view public returns(bool) {
    return widgetsByName[name].contractAddr == address(0);
  }

}
