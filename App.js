import React from 'react';
import { Platform, StatusBar, StyleSheet, View, AsyncStorage, Alert } from 'react-native';
import { AppLoading, Asset, Font, Icon } from 'expo';
import AppNavigator from './navigation/AppNavigator';
import { publicFileToIpfs, publicJsonToIpfs, getJsonFromIpfs } from './helpers/ipfsApi';
import nkn from 'nkn-client'
let NKN_CLIENT = {}



export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar barStyle="default" />}
          <AppNavigator />
        </View>
      );
    }
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require('./assets/images/robot-dev.png'),
        require('./assets/images/robot-prod.png'),
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Icon.Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      }),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };



  componentDidMount = async () => {
    try {
      console.log('Trying to open connection to NKN');
      const logged = await AsyncStorage.getItem('@NknStore:logged');
      if (logged === 'true') {
        console.log('logged');
        /*
        const ethAddress = await AsyncStorage.getItem('@NknStore:ethAddress');
        const ethPrivateKey = await AsyncStorage.getItem('@NknStore:ethPrivateKey');
        NKN_CLIENT = await nkn({
          identifier: ethAddress,
          privateKey: ethPrivateKey,
          seedRpcServerAddr: 'http://104.196.247.255:30003',
        });
        if (NKN_CLIENT !== {}) {
          NKN_CLIENT.on('connect', () => {
            console.log('Connection opened.');
            NKN_CLIENT.on('message', (src, payload, payloadType) => {
              // TODO: here make saving messages to chatHistory
              if (payloadType === nkn.PayloadType.TEXT) {
                console.log('Receive text message:', src, payload);
              } else if (payloadType === nkn.PayloadType.BINARY) {
                console.log('Receive binary message:', src, payload);
              }
            });
          });
        }
        */
      } else {
        Alert.alert('Not logged in','Please, log in!')
        // this.props.navigation.navigate('Login'); // gets err
      }
    } catch (e) {
      console.error('App componentDidMount',e);
    }
  }



}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
